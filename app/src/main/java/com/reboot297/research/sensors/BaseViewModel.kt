/*
 * Copyright (c) 2022. Viktor Pop
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.reboot297.research.sensors

import android.hardware.Sensor
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.lifecycle.ViewModel

/**
 * Base ViewModel
 */
abstract class BaseViewModel : ViewModel(), SensorEventListener {

    var sensorManager: SensorManager? = null
    val sensors = mutableMapOf<Int, Sensor?>()

    fun startTracking(sensorType: Int): Boolean {

        val sensor = sensorManager?.getDefaultSensor(sensorType)
        if (sensor != null) {
            sensors[sensorType] = sensor
            return sensorManager?.registerListener(
                this,
                sensor,
                SensorManager.SENSOR_DELAY_NORMAL
            ) ?: false
        }
        return false
    }

    fun getSensorInfo(type: Int): Sensor? {
        return sensorManager?.getDefaultSensor(type)
    }

    /**
     * Check if sensor listened
     */
    fun isSensorActive(type: Int): Boolean {
        return sensors[type] != null
    }

    fun stopTracking(sensorType: Int) {
        sensorManager?.unregisterListener(this, sensors.remove(sensorType))
    }

    fun stopTracking() {
        sensorManager?.unregisterListener(this)
    }
}
