/*
 * Copyright (c) 2022. Viktor Pop
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.reboot297.research.sensors.motion

import android.hardware.Sensor
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.reboot297.research.sensors.BaseSensorFragment
import com.reboot297.research.sensors.R
import com.reboot297.research.sensors.databinding.FragmentMotionBinding
import com.reboot297.research.sensors.databinding.LayoutAccelerometerBinding
import com.reboot297.research.sensors.databinding.LayoutLinearAccelerationBinding

/**
 * Fragment for motion sensors.
 */
class MotionFragment : BaseSensorFragment<FragmentMotionBinding>() {
    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentMotionBinding
        get() = FragmentMotionBinding::inflate

    private var _accelerometerBinding: LayoutAccelerometerBinding? = null
    private val accelerometerBinding: LayoutAccelerometerBinding
        get() = _accelerometerBinding!!

    private var _linearAccelerationBinding: LayoutLinearAccelerationBinding? = null
    private val linearAccelerationBinding: LayoutLinearAccelerationBinding
        get() = _linearAccelerationBinding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAccelerometerSensor(view)
        initLinearAccelerationSensor(view)
        initGravitySensor()
        initGyroscopeSensor()
        initStepCounterSensor()
    }

    private fun initAccelerometerSensor(view: View) {
        _accelerometerBinding = LayoutAccelerometerBinding.bind(view)
        initSwitch(
            accelerometerBinding.accelerometerView,
            accelerometerBinding.accelerometerXValueView,
            Sensor.TYPE_ACCELEROMETER
        )

        val unit = viewModel.metadataMap[Sensor.TYPE_ACCELEROMETER]?.unit?.let { getString(it) }

        viewModel.sensorsMap[Sensor.TYPE_ACCELEROMETER]?.observe(viewLifecycleOwner) { value ->
            accelerometerBinding.accelerometerXValueView.text =
                getString(R.string.accelerometer_value, "X", value[0], unit)
            accelerometerBinding.accelerometerYValueView.text =
                getString(R.string.accelerometer_value, "Y", value[1], unit)
            accelerometerBinding.accelerometerZValueView.text =
                getString(R.string.accelerometer_value, "Z", value[2], unit)
        }

        accelerometerBinding.accelerometerXValueView.setOnClickListener {
            findNavController().navigate(MotionFragmentDirections.actionToDetailsFragment(Sensor.TYPE_ACCELEROMETER))
        }
    }

    private fun initLinearAccelerationSensor(view: View) {
        _linearAccelerationBinding = LayoutLinearAccelerationBinding.bind(view)
        initSwitch(
            linearAccelerationBinding.linearAccelerationView,
            linearAccelerationBinding.linearAccelerationXValueView,
            Sensor.TYPE_LINEAR_ACCELERATION
        )

        val unit = viewModel.metadataMap[Sensor.TYPE_LINEAR_ACCELERATION]?.unit?.let { getString(it) }

        viewModel.sensorsMap[Sensor.TYPE_LINEAR_ACCELERATION]?.observe(viewLifecycleOwner) { value ->
            linearAccelerationBinding.linearAccelerationXValueView.text =
                getString(R.string.linear_acceleration_value, "X", value[0], unit)
            linearAccelerationBinding.linearAccelerationYValueView.text =
                getString(R.string.linear_acceleration_value, "Y", value[1], unit)
            linearAccelerationBinding.linearAccelerationZValueView.text =
                getString(R.string.linear_acceleration_value, "Z", value[2], unit)
        }

        linearAccelerationBinding.linearAccelerationXValueView.setOnClickListener {
            findNavController()
                .navigate(MotionFragmentDirections.actionToDetailsFragment(Sensor.TYPE_LINEAR_ACCELERATION))
        }
    }

    private fun initGravitySensor() {
        initSwitch(
            binding.gravityView,
            binding.gravityXValueView,
            Sensor.TYPE_GRAVITY
        )

        val unit = viewModel.metadataMap[Sensor.TYPE_GRAVITY]?.unit?.let { getString(it) }

        viewModel.sensorsMap[Sensor.TYPE_GRAVITY]?.observe(viewLifecycleOwner) { value ->
            binding.gravityXValueView.text = getString(R.string.gravity_value, "X", value[0], unit)
            binding.gravityYValueView.text = getString(R.string.gravity_value, "Y", value[1], unit)
            binding.gravityZValueView.text = getString(R.string.gravity_value, "Z", value[2], unit)
        }

        binding.gravityXValueView.setOnClickListener {
            findNavController().navigate(MotionFragmentDirections.actionToDetailsFragment(Sensor.TYPE_GRAVITY))
        }
    }

    private fun initGyroscopeSensor() {
        initSwitch(
            binding.gyroscopeView,
            binding.gyroscopeXValueView,
            Sensor.TYPE_GYROSCOPE
        )

        val unit = viewModel.metadataMap[Sensor.TYPE_GYROSCOPE]?.unit?.let { getString(it) }

        viewModel.sensorsMap[Sensor.TYPE_GYROSCOPE]?.observe(viewLifecycleOwner) { value ->
            binding.gyroscopeXValueView.text = getString(R.string.gyroscope_value, "X", value[0], unit)
            binding.gyroscopeYValueView.text =
                getString(R.string.gyroscope_value, "Y", value[1], unit)
            binding.gyroscopeZValueView.text = getString(R.string.gyroscope_value, "Z", value[2], unit)
        }

        binding.gyroscopeXValueView.setOnClickListener {
            findNavController().navigate(MotionFragmentDirections.actionToDetailsFragment(Sensor.TYPE_GYROSCOPE))
        }
    }

    private fun initStepCounterSensor() {
        initSwitch(
            binding.stepCounterView,
            binding.stepCounterValueView,
            Sensor.TYPE_STEP_COUNTER
        )

        val unit = viewModel.metadataMap[Sensor.TYPE_STEP_COUNTER]?.unit?.let { getString(it) }

        viewModel.sensorsMap[Sensor.TYPE_STEP_COUNTER]?.observe(viewLifecycleOwner) { values ->
            binding.stepCounterValueView.text = getString(R.string.step_counter_value, values[0], unit)
        }

        binding.stepCounterValueView.setOnClickListener {
            findNavController().navigate(MotionFragmentDirections.actionToDetailsFragment(Sensor.TYPE_STEP_COUNTER))
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _accelerometerBinding = null
        _linearAccelerationBinding = null
    }
}
