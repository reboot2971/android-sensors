/*
 * Copyright (c) 2022. Viktor Pop
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.reboot297.research.sensors

import android.hardware.Sensor
import android.hardware.SensorEvent
import androidx.lifecycle.MutableLiveData

const val COUNT_1 = 1
const val COUNT_3 = 3

/**
 * ViewModel with raw sensor data.
 * We have one viewModel for all sensors.
 */
open class SensorsViewModel : BaseViewModel() {

    /**
     * Map of live data objects.
     */
    val sensorsMap = mapOf(
        /**
         * Motion sensors.
         */
        Pair(Sensor.TYPE_ACCELEROMETER, MutableLiveData<FloatArray>()),
        Pair(Sensor.TYPE_LINEAR_ACCELERATION, MutableLiveData<FloatArray>()),
        Pair(Sensor.TYPE_GRAVITY, MutableLiveData<FloatArray>()),
        Pair(Sensor.TYPE_GYROSCOPE, MutableLiveData<FloatArray>()),
        Pair(Sensor.TYPE_STEP_COUNTER, MutableLiveData<FloatArray>()),

        /**
         * Position sensors.
         */
        Pair(Sensor.TYPE_PROXIMITY, MutableLiveData<FloatArray>()),
        Pair(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR, MutableLiveData<FloatArray>()),
        Pair(Sensor.TYPE_MAGNETIC_FIELD, MutableLiveData<FloatArray>()),

        /**
         * Environment sensors.
         */
        Pair(Sensor.TYPE_PRESSURE, MutableLiveData<FloatArray>()),
        Pair(Sensor.TYPE_AMBIENT_TEMPERATURE, MutableLiveData<FloatArray>()),
        Pair(Sensor.TYPE_LIGHT, MutableLiveData<FloatArray>()),
        Pair(Sensor.TYPE_RELATIVE_HUMIDITY, MutableLiveData<FloatArray>()),
    )

    /**
     * Map of metadata that cannot be extracted from sensor.
     */
    val metadataMap = mapOf(
        /**
         * Environment
         */
        Pair(
            Sensor.TYPE_PRESSURE,
            SensorMetadata(R.string.unit_pressure, COUNT_1, R.string.description_pressure)
        ),
        Pair(
            Sensor.TYPE_AMBIENT_TEMPERATURE,
            SensorMetadata(R.string.unit_celcius, COUNT_1, R.string.description_temperature)
        ),
        Pair(
            Sensor.TYPE_LIGHT,
            SensorMetadata(R.string.unit_light, COUNT_1, R.string.description_light)
        ),
        Pair(
            Sensor.TYPE_RELATIVE_HUMIDITY,
            SensorMetadata(R.string.unit_humidity, COUNT_1, R.string.description_humidity)
        ),
        /**
         * Position
         */
        Pair(
            Sensor.TYPE_PROXIMITY,
            SensorMetadata(R.string.unit_cm, COUNT_1, R.string.description_proximity)
        ),
        Pair(
            Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR,
            SensorMetadata(null, COUNT_3, R.string.description_geo_magnetic_rotation_vector)
        ),
        Pair(
            Sensor.TYPE_MAGNETIC_FIELD,
            SensorMetadata(R.string.unit_microtesla, COUNT_3, R.string.description_magnetic_field)
        ),
        /**
         * Motion
         */
        Pair(
            Sensor.TYPE_ACCELEROMETER,
            SensorMetadata(
                R.string.unit_meter_per_sec_squared,
                COUNT_3,
                R.string.description_accelerometer
            )
        ),
        Pair(
            Sensor.TYPE_LINEAR_ACCELERATION,
            SensorMetadata(
                R.string.unit_meter_per_sec_squared,
                COUNT_3,
                R.string.description_linear_acceleration
            )
        ),
        Pair(
            Sensor.TYPE_GRAVITY,
            SensorMetadata(
                R.string.unit_meter_per_sec_squared,
                COUNT_3,
                R.string.description_gravity
            )
        ),
        Pair(
            Sensor.TYPE_GYROSCOPE,
            SensorMetadata(R.string.unit_rad_per_sec, COUNT_3, R.string.description_gyroscope)
        ),
        Pair(
            Sensor.TYPE_STEP_COUNTER,
            SensorMetadata(R.string.unit_steps, COUNT_1, R.string.description_step_counter)
        )
    )

    override fun onSensorChanged(event: SensorEvent?) {

        when (event?.sensor?.type) {
            Sensor.TYPE_PRESSURE,
            Sensor.TYPE_AMBIENT_TEMPERATURE,
            Sensor.TYPE_LIGHT,
            Sensor.TYPE_RELATIVE_HUMIDITY,

            Sensor.TYPE_PROXIMITY,
            Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR,
            Sensor.TYPE_MAGNETIC_FIELD,

            Sensor.TYPE_ACCELEROMETER,
            Sensor.TYPE_LINEAR_ACCELERATION,
            Sensor.TYPE_GRAVITY,
            Sensor.TYPE_GYROSCOPE,
            Sensor.TYPE_STEP_COUNTER -> {
                updateSensorData(event)
            }
        }
    }

    private fun updateSensorData(event: SensorEvent?) {
        event?.sensor?.type.let { type ->
            event?.values?.let { values ->
                sensorsMap[type]?.postValue(values)
            }
        }
    }

    @Suppress("EmptyFunctionBlock")
    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }
}
